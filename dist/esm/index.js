import soapClient from "./soapClient.js";
import { generateAuditRecord, FileEngine, } from "@digigov-oss/gsis-audit-record-db";
import config from "./config.json";
/**
 * @param passportNumber
 * @param reasonDesc
 * @param user username
 * @param pass password
 * @param overrides options overrides
 * @returns AuditRecord | errorRecord
 */
export const findPassportByPassportNumber = async (passportNumber, reasonDesc, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error("Audit record is not initialized");
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const response = await s.findPassportByPassportNumber(passportNumber, reasonDesc);
        return { ...response, ...auditRecord };
    }
    catch (error) {
        throw error;
    }
};
/**
 * @param name
 * @param surname
 * @param dateOfBirth
 * @param reasonDesc
 * @param user username
 * @param pass password
 * @param overrides options overrides
 * @returns AuditRecord | errorRecord
 */
export const findPassportByNameSurnameAndDateOfBirth = async (name, surname, dateOfBirth, reasonDesc, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error("Audit record is not initialized");
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const response = await s.findPassportByNameSurnameAndDateOfBirth(name, surname, dateOfBirth, reasonDesc);
        return { ...response, ...auditRecord };
    }
    catch (error) {
        throw error;
    }
};
export default { findPassportByPassportNumber, findPassportByNameSurnameAndDateOfBirth };
