import { AuditRecord, AuditEngine } from "@digigov-oss/gsis-audit-record-db";
export declare type AuditInit = AuditRecord;
export declare type Passport = {
    arithmosDiavatiriou: string;
    imerominiaLiksis: string;
    imerominiaEkdosis: string;
    onoma: string;
    onomaEn: string;
    patronymo: string;
    mitronymo: string;
    fylo: string;
    toposGennisis: string;
    status: string;
    statusCode: number;
    arithmosTaytotitas: string;
    eidosTaytotitas: string;
    imerominiaGennisis: string;
    eponymoEn: string;
    eponymo: string;
};
export declare type FindPassportByParametersOutputRecord = {
    status: string;
    diavatiria: Passport[];
};
export declare type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export declare type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 * @param passportNumber
 * @param reasonDesc
 * @param user username
 * @param pass password
 * @param overrides options overrides
 * @returns AuditRecord | errorRecord
 */
export declare const findPassportByPassportNumber: (passportNumber: string, reasonDesc: string, user: string, pass: string, overrides?: Overrides) => Promise<{
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    status: string;
    diavatiria: Passport[];
} | {
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    errorCode: string;
    errorDescr: string;
}>;
/**
 * @param name
 * @param surname
 * @param dateOfBirth
 * @param reasonDesc
 * @param user username
 * @param pass password
 * @param overrides options overrides
 * @returns AuditRecord | errorRecord
 */
export declare const findPassportByNameSurnameAndDateOfBirth: (name: string, surname: string, dateOfBirth: string, reasonDesc: string, user: string, pass: string, overrides?: Overrides) => Promise<{
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    status: string;
    diavatiria: Passport[];
} | {
    auditUnit?: string | undefined;
    auditTransactionId?: string | undefined;
    auditProtocol?: string | undefined;
    auditTransactionDate?: string | undefined;
    auditUserIp?: string | undefined;
    auditUserId?: string | undefined;
    errorCode: string;
    errorDescr: string;
}>;
declare const _default: {
    findPassportByPassportNumber: (passportNumber: string, reasonDesc: string, user: string, pass: string, overrides?: Overrides | undefined) => Promise<{
        auditUnit?: string | undefined;
        auditTransactionId?: string | undefined;
        auditProtocol?: string | undefined;
        auditTransactionDate?: string | undefined;
        auditUserIp?: string | undefined;
        auditUserId?: string | undefined;
        status: string;
        diavatiria: Passport[];
    } | {
        auditUnit?: string | undefined;
        auditTransactionId?: string | undefined;
        auditProtocol?: string | undefined;
        auditTransactionDate?: string | undefined;
        auditUserIp?: string | undefined;
        auditUserId?: string | undefined;
        errorCode: string;
        errorDescr: string;
    }>;
    findPassportByNameSurnameAndDateOfBirth: (name: string, surname: string, dateOfBirth: string, reasonDesc: string, user: string, pass: string, overrides?: Overrides | undefined) => Promise<{
        auditUnit?: string | undefined;
        auditTransactionId?: string | undefined;
        auditProtocol?: string | undefined;
        auditTransactionDate?: string | undefined;
        auditUserIp?: string | undefined;
        auditUserId?: string | undefined;
        status: string;
        diavatiria: Passport[];
    } | {
        auditUnit?: string | undefined;
        auditTransactionId?: string | undefined;
        auditProtocol?: string | undefined;
        auditTransactionDate?: string | undefined;
        auditUserIp?: string | undefined;
        auditUserId?: string | undefined;
        errorCode: string;
        errorDescr: string;
    }>;
};
export default _default;
