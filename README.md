# Client for findPassportByParameters service of KED

Client to connect to findPassportByParameters service, useful for nextjs/nodejs projects.

#### Example:
```
import findPassportByPassportNumber from '@digigov-oss/find-passport-by-parameters-client';

const test = async () => {
   const overrides = {
        prod:false,
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        auditStoragePath: '/auditStorage',
    }

    try {
        return await findPassportByPassportNumber("AB1234567", "username", "password", overrides);
    } catch (error) {
        console.log(error);
    }
}

test().then((r) => { console.log('findPassportByPassportNumber', r); });
```
* you can use `overrides` to override the default values
* for your tests, you don't need to use the `overrides` mecahnism,in that case, the default storage path will be used ie `/tmp`
* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine.
```
import findPassportByPassportNumber from '@digigov-oss/find-passport-by-parameters-client';
import {PostgreSqlEngine} from '@digigov-oss/gsis-audit-record-db'; 

const test = async () => {
    try {
        const overrides = {
        auditEngine: new PostgreSqlEngine('postgres://postgres:postgres@localhost:5432/postgres'),
        auditInit: {
            auditUnit: 'grnet.gr',
        },
        }
        return await findPassportByPassportNumber("AB1234567", "username", "password", overrides);
    } catch (error) {
        console.log(error);
    }
}

test().then((r) => { console.log('findPassportByPassportNumber', r); });

```

Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

If you plan to use only the `FileEngine`, you can skip the installation of other engines by ignoring optional dependencies.
i.e.` yarn install --ignore-optional`

#### Returns
an object like the following:
```
TODO
```
or an error message like:
```
TODO
```

#### Available data for testing:
000152418

##### * Notes
you have to ask KED for policePassportService_v1.3 documentation to get more info about the output and error fields.

#### * known issues
If KED advertises a wrong endpoint for the service on production WSDL, you can override the endpoint on the `overrides` object.

```
const overrides = {
    endpoint: 'my new endpoint here',
}
```
