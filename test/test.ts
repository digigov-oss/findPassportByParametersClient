import { findPassportByPassportNumber, findPassportByNameSurnameAndDateOfBirth } from "../src/index";
import config from "./config.json";
import inspect from 'object-inspect';

const test1 = async () => {
  try {
    return await findPassportByPassportNumber(
      "AB1234567",
      "TEST",
      config.user,
      config.pass
    );
  } catch (error) {
    console.log(error);
  }
};

const test2 = async () => {
  try {
    return await findPassportByNameSurnameAndDateOfBirth(
      "ΤΕΣΤΙΟΣ",
      "ΤΕΣΤΙΟΠΟΥΛΟΣ",
      "11/11/1980",
      "TEST",
      config.user,
      config.pass
    );
  } catch (error) {
    console.log(error);
  }
};

test1().then((r) => {
  console.log("findPassportByPassportNumber", inspect(r,{depth:10,indent:"\t"}));
});

test2().then((r) => {
  console.log("findPassportByNameSurnameAndDateOfBirth", inspect(r,{depth:10,indent:"\t"}));
});
