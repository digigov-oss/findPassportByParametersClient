import soapClient from "./soapClient.js";
import {
  generateAuditRecord,
  AuditRecord,
  FileEngine,
  AuditEngine,
} from "@digigov-oss/gsis-audit-record-db";

import config from "./config.json";

export type AuditInit = AuditRecord;

export type Passport = {
    arithmosDiavatiriou: string;
    imerominiaLiksis: string;  
    imerominiaEkdosis: string;  
    onoma: string;  
    onomaEn: string;   
    patronymo: string;  
    mitronymo: string;  
    fylo: string;  //ΓΥΝΑΙΚΑ
    toposGennisis: string;  
    status: string;  
    statusCode: number;
    arithmosTaytotitas:  string;  
    eidosTaytotitas:  string; //'ΠΟΛΙΤΙΚΗ'
    imerominiaGennisis: string;
    eponymoEn: string;   
    eponymo: string;
}

export type FindPassportByParametersOutputRecord = {
  status: string;
  diavatiria: Passport[];
};

export type ErrorRecord = {
  errorCode: string;
  errorDescr: string;
};

/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type Overrides = {
  endpoint?: string;
  prod?: boolean;
  auditInit?: AuditRecord;
  auditStoragePath?: string;
  auditEngine?: AuditEngine;
};

/**
 * @param passportNumber
 * @param reasonDesc
 * @param user username
 * @param pass password
 * @param overrides options overrides
 * @returns AuditRecord | errorRecord
 */
export const findPassportByPassportNumber = async (
  passportNumber: string,
  reasonDesc: string,
  user: string,
  pass: string,
  overrides?: Overrides
) => {
  const endpoint = overrides?.endpoint ?? "";
  const prod = overrides?.prod ?? false;
  const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
  const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
  const auditEngine =
    overrides?.auditEngine ?? new FileEngine(auditStoragePath);
  const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
  const auditRecord = await generateAuditRecord(auditInit, auditEngine);
  if (!auditRecord) throw new Error("Audit record is not initialized");

  try {
    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response:
      | FindPassportByParametersOutputRecord
      | ErrorRecord = await s.findPassportByPassportNumber(passportNumber, reasonDesc);
    return { ...response, ...auditRecord };
  } catch (error) {
    throw error;
  }
};

/**
 * @param name
 * @param surname
 * @param dateOfBirth
 * @param reasonDesc
 * @param user username
 * @param pass password
 * @param overrides options overrides
 * @returns AuditRecord | errorRecord
 */
export const findPassportByNameSurnameAndDateOfBirth = async (
  name: string,
  surname: string,
  dateOfBirth: string,
  reasonDesc: string,
  user: string,
  pass: string,
  overrides?: Overrides
) => {
  const endpoint = overrides?.endpoint ?? "";
  const prod = overrides?.prod ?? false;
  const auditInit = overrides?.auditInit ?? ({} as AuditRecord);
  const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
  const auditEngine =
    overrides?.auditEngine ?? new FileEngine(auditStoragePath);
  const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
  const auditRecord = await generateAuditRecord(auditInit, auditEngine);
  if (!auditRecord) throw new Error("Audit record is not initialized");

  try {
    const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
    const response:
      | FindPassportByParametersOutputRecord
      | ErrorRecord = await s.findPassportByNameSurnameAndDateOfBirth(name, surname, dateOfBirth, reasonDesc);
    return { ...response, ...auditRecord };
  } catch (error) {
    throw error;
  }
};

export default { findPassportByPassportNumber, findPassportByNameSurnameAndDateOfBirth };
